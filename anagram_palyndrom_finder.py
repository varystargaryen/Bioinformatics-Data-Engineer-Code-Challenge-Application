'''Script to parse any given HTML webpage into formatted text and
check if any words on the webpage are palindromes.'''
import sys
import requests
from bs4 import BeautifulSoup
from analyze_string import Palindrome
WEB_DATA = requests.get(sys.argv[1]).content
TEMP = BeautifulSoup(WEB_DATA, 'html.parser')
WEBTEXT = TEMP.find_all(text=True)
CLEANED_TEXT = ''
IGNORED = ['[document]', 'head']
for element in WEBTEXT:
    if element.parent.name not in IGNORED:
        CLEANED_TEXT += '{} '.format(element)
WHITELISTED = set('ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz')
CLEANED_TEXT = "".join(filter(WHITELISTED.__contains__, CLEANED_TEXT)).split()
PALINDROME_FINDER = Palindrome(CLEANED_TEXT)
PALINDROME_FINDER.check_palindrome()
if len(PALINDROME_FINDER.palindromes) > 0:
    print("\nThe palindromes found on this webpage are: ")
    for word in PALINDROME_FINDER.palindromes:
        print(word)
else:
    print("There are no palindromes found in this webpage.")
