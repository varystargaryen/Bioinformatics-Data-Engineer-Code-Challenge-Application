'''Module containing 3 classes to analyze a text file and perform various string operations, as
well as creating anagrams and checking if a string is a palindrome.'''
import itertools
class AnalyzeString:
    '''Contains functions to perform various string manipulations, such as loading from a
    file, writing to a file, appending a substring, removing a substring, and mirroring a string.'''
    def __init__(self, contents=''):
        self.contents = contents

    def load_file(self, filename):
        '''Loads a text file with the given file name, if one exists.'''
        with open(filename, 'r') as textfile:
            self.contents = textfile.read()

    @staticmethod
    def write_file(file_to_write, contents):
        '''Writes a given string to a file, with a given name.
        Creates a file with the given name if none exists.'''
        with open(file_to_write, 'w') as textfile:
            textfile.write(contents)

    def append(self, to_append):
        '''Appends a given string to the end of the string contents of the object.'''
        self.contents += to_append

    def remove_substring(self, to_remove):
        '''Removes a given substring within a string.'''
        self.contents = self.contents.replace(to_remove, '')

    @staticmethod
    def mirror(original):
        '''Reverses and returns the new string.'''
        return original[::-1]

class Anagram(AnalyzeString):
    '''Child class of AnalyzeString that can create anagrams of a word through permutations.'''
    def __init__(self, contents=''):
        super().__init__(contents)
        self.anagrams = []

    def create_anagrams(self):
        '''Creates anagrams of each word in the contents of the object by generating
        all permutations of the word as a set to remove duplicates.'''
        self.anagrams = list(itertools.permutations(list(self.contents)))
        self.anagrams = [''.join(phrase) for phrase in self.anagrams]
        self.anagrams = set(self.anagrams)

class Palindrome(Anagram):
    '''Child of Anagram class that can determine if a
    word or any anagram of it is also a palindrome.'''
    def __init__(self, contents=''):
        super().__init__(contents)
        self.palindromes = set()

    def find_if_anagram_is_palindrome(self):
        '''Creates all anagrams for each stored word and compares the word
        to its mirror to check if it is a palindrome.'''
        self.create_anagrams()
        for phrase in self.anagrams:
            if phrase == self.mirror(phrase) and len(phrase) > 1:
                self.palindromes.add(phrase.lower())

    def check_palindrome(self):
        '''Checks if the contents of the object contains a string that is a palindrome
        by comparing the original string to its mirror'''
        for word in self.contents:
            if word == self.mirror(word) and len(word) > 1:
                self.palindromes.add(word.lower())
